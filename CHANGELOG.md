## 1.15.10 / 2019.04.05

#### Enhancement

- Fix retry, in case the error was 502 the retry would not work 😔

## 1.15.9 / 2019.04.05

##### Dependency

- Update [@types/node 11.13.0](http://definitelytyped.org/) 🚀
- Update [babel 7.4.3](https://github.com/babel/babel/releases) 🚀
- Update [eslint 5.16.0](https://github.com/eslint/eslint/releases) 🚀
- Update [typescript 3.4.1](https://github.com/Microsoft/TypeScript/releases) 🚀

## 1.15.8 / 2019.04.05

#### Enhancement

- Add public list methods to track and sessions 🚌
- Fix invite meethods 👫️

## 1.15.7 / 2019.03.26

#### Enhancement

- Fix retry to not generate the URL each time it retry, duplicating the parameters each time 👯‍♀️

## 1.15.4 - 1.15.6 / 2019.03.26

#### Enhancement

- Hotfix SDK X-Client version Ẍ

## 1.15.3 / 2019.03.26

#### Enhancement

- Hotfix to add back the Finance Modifier class 🏷

## 1.15.1 - 1.15.2 / 2019.03.25

#### Enhancement

- Add project with role option 🤴🏻

## 1.15.0 / 2019.03.23

#### Enhancement

- All SDK is now converted to use Classes 🏆
- Add retry option ♽

## 1.14.3 / 2019.02.28

#### Enhancement

- Add public file upload parameter 🌅
- Update ESLint to EcmaVersion 10 🔝
- Remove log counter 👴🏻
- Stats NPM vulnerabilities: 0 👮🏻‍♂️
- Stats Lint errors: clean

##### Dependency

- Update [@types/node 11.9.5](http://definitelytyped.org/) 🚀
- Update [babel 7.3.4](https://github.com/babel/babel/releases) 🚀
- Update [typescript 3.3.3333](https://github.com/Microsoft/TypeScript/releases) 🚀
- Remove Chai 🚀
- Remove Mocha 🚀
- Remove Yarn 🚀

## 1.14.2 / 2019.02.19

#### Enhancement

- Add publicList and publicGet methods for submissions 📩

## 1.14.1 / 2019.02.19

#### Enhancement

- Fix version 👮🏻‍♂️

## 1.14.0 / 2019.02.19

#### Enhancement

- Migrate to Type Script 🤖

##### Dependency

- Add [gulp-typescript 5.0.0](https://github.com/ivogabe/gulp-typescript/releases) 🚀
- Add [typescript 3.3.3](https://github.com/Microsoft/TypeScript/releases) 🚀
- Add [@types/node 11.9.4](http://definitelytyped.org/) 🚀
- Update [babel 7.3.3](https://github.com/babel/babel/releases) 🚀
- Update [eslint 5.14.1](https://github.com/eslint/eslint/releases) 🚀
- Update [mocha 6.0.0](https://github.com/mochajs/mocha/releases) 🚀

## 1.13.8 / 2019.02.11

#### Enhancement

- Change product public list to list only the own product and child products but not siblings 👭

##### Dependency

- Update [eslint-config-google 0.12.0](https://github.com/google/eslint-config-google/releases) 🚀
- Update [eslint 5.12.1](https://github.com/eslint/eslint/releases) 🚀

## 1.13.5 - 1.13.7 / 2019.01.29

#### Enhancement

- Add paypal execute method 💰

##### Dependency

- Update to NPM [babel 7.3.1](https://github.com/babel/babel/releases) 🚀
- Update [eslint 5.12.1](https://github.com/eslint/eslint/releases) 🚀

## 1.13.4 / 2018.12.22

#### Enhancement

- Update ecma version to 2017 🔝
- Fix null issue on Express JS for DELETE verb 👨🏻‍🚒

## 1.13.2 / 2018.11.21

#### Enhancement

- Enhance list and count method for app users 😀

## 1.13.1 / 2018.11.20

#### Enhancement

- Add methods to notify voucher owner and recipient 💬

## 1.13.0 / 2018.11.17

#### Enhancement

- Add promise to all methods 🙏
- Remove all lint errors 🔥

##### Dependency

- Update to NPM [gulp-babel 8.0.0](https://github.com/babel/gulp-babel/releases) 🚀
- Update [eslint-config-google 0.11.0](https://github.com/google/eslint-config-google/releases) 🚀

## 1.12.15 / 2018.11.11

#### Enhancement

- Add generate_badge method to user 🎫

## 1.12.12 - 1.12.14 / 2018.10.31

#### Enhancement

- Add generate_card method to user 💌

## 1.12.8 - 1.12.11 / 2018.10.02

#### Enhancement

- Add matchmaking buyer and spot

## 1.12.7 / 2018.09.27

#### Enhancement

- Fix updates

## 1.12.6 / 2018.09.27

#### Enhancement

- Add device class 🤖
- Add matchmaking class 🤝
- Add trigger class 🧙🏻‍♂️

##### Development Dependency

- Update [chai 4.2.0](https://github.com/chaijs/chai/releases) 🚀

## 1.12.3 - 1.12.5 / 2018.09.24

#### Enhancement

- Add recognition class 🤴🏻

## 1.12.3 / 2018.09.24

#### Enhancement

- Add recognition class 🤴🏻

## 1.12.2 / 2018.09.17

#### Enhancement

- Add exhibitor class 👨‍🏫
- Update locations class to new model 🏟

## 1.12.1 / 2018.09.14

#### Enhancement

- Fix user list count 🕵🏻‍♂️

## 1.12.0 / 2018.09.14

#### Enhancement

- Move from event module to product ✅

## 1.11.5 / 2018.08.28

#### Enhancement

- Move from forms module to survey ✅

## 1.11.2 - 1.11.4 / 2018.08.23

#### Enhancement

- Add admin module in the sdk for root submodules

## 1.11.1 / 2018.08.20

#### Enhancement

- Update notifications list method

##### Dependency

- Update [eslint 5.4.0](https://github.com/eslint/eslint/releases) 🚀

## 1.11.0 / 2018.08.14

#### Enhancement

- Add Time-Zone non-standard HTTP header

## 1.10.13 / 2018.08.08

#### Enhancement

- Add new file upload parameter 🔝

## 1.10.12 / 2018.08.06

#### Enhancement

- Add Forms answers list and count methods ☑️

## 1.10.11 / 2018.08.06

#### Enhancement

- Add Forms template methods ☑️

##### Dependency

- Update [eslint 5.3.0](https://github.com/eslint/eslint/releases) 🚀

## 1.10.4 - 1.10.10 / 2018.08.02

#### Enhancement

- Add Forms methods

## 1.10.1 - 1.10.3 / 2018.07.24

##### Dependency

- Update [eslint 5.2.0](https://github.com/eslint/eslint/releases) 🚀

## 1.10.0 / 2018.07.19

#### Enhancement

- Drop support for NodeJS <= 4
- Add list paper reviewers method

##### Dependency

- Update [eslint 5.1.0](https://github.com/eslint/eslint/releases) 🚀
- Update [eslint-watch 4.0.2](https://github.com/rizowski/eslint-watch/releases)

## 1.9.139 / 2018.07.04

#### Enhancement

- Make user creation and user project add methods that does not require authentication token

## 1.9.138 / 2018.06.30

#### Enhancement

- Add event based information to the public event page

##### Dependency

- Update [eslint-watch 4.0.0](https://github.com/rizowski/eslint-watch/releases)
- Update [eslint 5.0.1](https://github.com/eslint/eslint/releases) 🚀

## 1.9.137 / 2018.06.29

#### Enhancement

- Add event based information to the public event page

## 1.9.136 / 2018.06.28

#### Enhancement

- Fix addProject to user

## 1.9.135 / 2018.06.28

#### Enhancement

- Move ConsumerProfile to standard class mode 🚩
- Fix many ESLint errors (reduced from 330 to 95) 🎉

## 1.9.134 / 2018.06.27

#### Enhancement

- Update ESLint 5.0.1

## 1.9.133 / 2018.06.25

#### Enhancement

- Update ESLint 5.0.0

## 1.9.132 / 2018.06.25

#### Enhancement

- Small fixes 💉
- Debug version and documentation 🚧

## 1.9.131 / 2018.06.21

#### Enhancement

- Add public list methods for sponsor and sponsor category 👀

## 1.9.129 - 1.9.130 / 2018.06.19

#### Enhancement

- Add addProject method to users

## 1.9.127 - 1.9.128 / 2018.06.18

#### Enhancement

- Move from sponsor contract to sponsor category 🤑

## 1.9.124 - 1.9.126 / 2018.06.15

#### Enhancement

- Minor fix

## 1.9.123 / 2018.06.15

#### Enhancement

- Add paper settings

## 1.9.19 - 1.9.121 / 2018.06.13

#### Enhancement

- Automate build versioning and npm/bower publishing

## 1.9.18 / 2018.06.13

#### Enhancement

- Added paper label methods

## 1.9.17 / 2018.06.05

#### Enhancement

- Fix count methods

## 1.9.0 - 1.9.16 / 2018.06.04

#### Enhancement

- Add UMD to support CommonJS, AMD and Script tags
- Add support to promises
- Publish library to NPM as farol-sdk
- Rename Bower library to farol-sdk

## 1.8.38 / 2018.05.21

#### Enhancement

- Add the ability to upload different files

## 1.8.37 / 2018.05.21

#### Enhancement

- Add user shouldUpdate method

## 1.8.33 - 1.8.36 / 2018.05.16

#### Enhancement

- Fix array parameters

## 1.8.31 - 1.8.32 / 2018.05.14

#### Enhancement

- Add X-Client-Version header

## 1.8.30 / 2018.05.14

#### Enhancement

- Fix passing array parameters for the API

## 1.8.29 / 2018.05.12

#### Enhancement

- Remove SDK force file name and let server define the download file name

## 1.8.25 / 2018.05.09

#### Enhancement

- Always send locale header

## 1.8.21 - 1.8.24 / 2018.05.09

#### Enhancement

- Fix return of string errors

## 1.8.20 / 2018.05.03

#### Enhancement

- Fix get url

## 1.8.18 - 1.8.19 / 2018.05.03

#### Enhancement

- Fix image download

## 1.8.17 / 2018.05.01

#### Enhancement

- Added user_id to get method

## 1.8.16 / 2018.05.01

#### Enhancement

- Added parameters to listMine method

## 1.8.15 / 2018.04.27

#### Enhancement

- Fix query string parameters

## 1.8.14 / 2018.04.26

#### Enhancement

- Removed appCode from user confirmation email

## 1.8.6 - 1.8.7 / 2018.04.18

#### Enhancement

- Add certification module

## 1.8.5 / 2018.04.18

#### Enhancement

- Add paper module

## 1.8.3 - 1.8.4 / 2018.04.18

#### Enhancement

- Add mine methods

## 1.8.2 / 2018.04.17

#### Enhancement

- Add getByExtId methods

## 1.8.0 - 1.8.1 / 2018.04.10

#### Enhancement

- Add Postman integration support
- Add saveByExtId methods

## 1.7.6 - 1.7.7 / 2018.04.10

#### Enhancement

- Add voucher and coupons list and count methods

## 1.7.4 - 1.7.5 / 2018.04.10

#### Enhancement

- Add user reset password and change password methods

## 1.7.3 / 2018.04.03

#### Fix

- Re-ran gulp, session and tracks were missing

## 1.7.2 / 2018.04.02

#### Enhancement

- Add session and track classes

## 1.7.1 / 2018.03.29

#### Enhancement

- Add sponsor and sponsor category classes

## 1.7.0 / 2018.03.29

#### Enhancement

- Fix babel parameter on gulp build process
- Add abstract class to replicated methods
- Make Event Speaker entity use abstract class

## 1.6.27 / 2018.03.28

#### Enhancement

- Added babel to the gulp build process

## 1.6.26 / 2018.03.20

#### Fix

- Quick fix on order change status method

## 1.6.25 / 2018.03.20

#### Fix

- Quick fix on order change status method

## 1.6.24 / 2018.03.20

#### Fix

- Quick fix on order change status method

## 1.6.23 / 2018.03.19

#### Enhancements

- Add order change status method

## 1.6.22 / 2018.03.16

#### Enhancements

- Add registration checkin method

## 1.6.16 .. 1.6.21 / 2018.03.12

#### Fix

- Fix filter on count for methods transaction, order and registration
- Add blob response type to donwload xslx files

## 1.6.15 / 2018.03.01

#### Fix

- registration.js file was missing on gulpfile.js

## 1.6.14 / 2018.03.01

#### Fix

- registration.js file was missing

## 1.6.13 / 2018.03.01

#### Enhancement

- Added registration list and count methods

## 1.6.12 / 2018.02.22

#### Fix

- Fix minor glitch on previous enhancement

## 1.6.11 / 2018.02.22

#### Enhancements

- Added update method to Event

## 1.6.10 / 2018.02.11

#### Enhancements

- Remove Finance Modifier public methods

## 1.6.9 / 2018.02.09

#### Enhancements

- Add public get url

## 1.6.8 / 2018.02.08

#### Enhancements

- Add Consumer Profile Finance methods

## 1.6.7 / 2018.02.07

#### Enhancements

- Add get url to file method
- Add order list by user

## 1.6.2 .. 1.6.6 / 2018.02.01

#### Enhancements

- Enhance callback method logic on all methods
- Fix paper and paper category methods

## 1.6.1 / 2018.02.01

#### Enhancements

- Merge

## 1.6.0 / 2018.01.31

#### Enhancements

- Add paper and paper category
- Enhance getHeaders logic on all methods

#### Dependencies

- Update [Pump 3.0.0](https://github.com/mafintosh/pump)

## 1.5.20 / 2018.01.17

#### Enhancements

- Updated Finance.Modifier.List method to accept pagination parameters

## 1.5.19 / 2018.01.07

#### Fix

- Fix transaction create method

## 1.5.18 / 2018.01.07

#### Fix

- Remove save user method and fix user create and update methods

## 1.5.17 / 2018.01.07

#### Fix

- Correction of the previous correction

## 1.5.16 / 2018.01.07

#### Fix

- Corrections to the user methods

## 1.5.12 .. 1.5.15 / 2018.01.06

#### Fix

- Add Transaction service

## 1.5.11 / 2018.01.05

#### Fix

- Previous version was broken

## 1.5.10 / 2018.01.05

#### Enhancements

- Add create user method

## 1.5.0 .. 1.5.9 / 2017.12.30

#### Enhancements

- Change the callback method to be inferred as the first method passed to any API method
- Add count methods to speakers, apps and accounts

## 1.4.13 / 2017.12.26

#### Fix

- changed speaker list to /event/speaker call and removed the event_id filter

## 1.4.12 / 2017.12.26

#### Fix

- added dummy pageSize and pageNumber to speaker list method to make it compliant with other methods

## 1.4.11 / 2017.12.26

#### Fix

- speaker.js not listed on gulp task on previous version

## 1.4.10 / 2017.12.26

#### Fix

- speaker.js file missing on previous version

## 1.4.9 / 2017.12.26

#### Enhancements

- Added list, get, create, update, remove methods for speaker

## 1.4.8 / 2017.12.18

#### Enhancements

- Added get user method

## 1.4.7 / 2017.12.18

#### Enhancements

- Added the update capability to the save user method

## 1.4.6 / 2017.12.17

#### Enhancements

- Adde Events public list method

## 1.4.5 / 2017.12.17

#### Enhancements

- Of course, a correction to the previous added method

## 1.4.4 / 2017.12.17

#### Enhancements

- Added account get method

## 1.4.3 / 2017.12.15

#### Enhancements

- Added event/event get method

## 1.4.2 / 2017.12.14

#### Enhancements

- Fix environments based API URL

## 1.4.1 / 2017.12.14

#### Enhancements

- Add FAROL_ENV and FAROL_API_URL variables to load API address

## 1.4.0 / 2017.12.14

#### Enhancements

- Enhance category list
- Add events namespace
- Remove unused code

#### Dependencies

- Update [Pump 2.0.0](https://github.com/mafintosh/pump)

## 1.3.76 / 2017.12.13

#### Enhancements

- Add event category list, count and save methods

## 1.3.16 / 2017.11.29

#### Fix

- Add project/app list method

## 1.3.14 ... 1.3.15 / 2017.11.21

#### Fix

- Add use method to fin modifier

## 1.3.11 ... 1.3.13 / 2017.11.20

#### Fix

- Fix Finance list orders method

## 1.3.8 ... 1.3.10 / 2017.11.17

#### Fix

- Fix Finance modifier methods

## 1.3.7 / 2017.11.17

#### Enhancements

- Add Finance modifier methods

## 1.3.6 / 2017.11.06

#### Fix

- Add Finance micro-service methods

## 1.3.5 / 2017.11.06

#### Fix

- Add get app details by code method

## 1.3.4 / 2017.11.06

#### Fix

- Add get app details by code method

## 1.3.3 / 2017.11.04

#### Fix

- Fix Account save method

## 1.3.2 / 2017.11.04

#### Fix

- Fix Account save method

## 1.3.1 / 2017.11.04

#### Fix

- Fix gulp build process

## 1.3.0 / 2017.11.03

#### Enhancement

- Rename lib folder to src
- Start to change NPM for YARN
- Add Account model
