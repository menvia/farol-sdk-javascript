namespace FarolSDK {
  export namespace Admin {
    export class Analytics extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('analytics');
      }

      online(args) {
        const method = this.getMethod('online');
        return FarolSDK.get(method, args, null, true);
      }

      download(...args) {
        const method = this.getMethod('download');
        return FarolSDK.get(method, args, null, true);
      }

      count(action, entityId, groupby, timeStart, timeEnd, ...args) {
        const data = {
          action: action,
          entity_id: entityId,
          groupby: groupby,
          time_start: timeStart,
          time_end: timeEnd,
        };
        const method = this.getMethod('count');
        return FarolSDK.get(method, args, data, true);
      }
    }
  }
}
