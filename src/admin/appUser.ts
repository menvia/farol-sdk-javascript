namespace FarolSDK {
  export namespace Admin {
    export class AppUser extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('app_user');
      }

      list(params, ...args) {
        const data = params || {};
        const method = this.getMethod('list');
        return FarolSDK.get(method, args, data, true);
      }

      count(params, ...args) {
        const data = params || {};
        const method = this.getMethod('count');
        return FarolSDK.get(method, args, data, true);
      }

      listSelectedAppUsers(ids, ...args) {
        const data = {
          ids: ids,
        };
        const method = this.getMethod('download/selectedAppUsers');
        return FarolSDK.get(method, args, data, true);
      }

      // Forces app user to update its info the next time it logs in the app
      updateInfo(appUser, ...args) {
        const data = {
          user: appUser,
        };
        const method = this.getMethod();
        return FarolSDK.post(method, args, data, true);
      }
    }
  }
}
