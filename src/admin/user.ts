namespace FarolSDK {
  export namespace Admin {
    export class User extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('user');
      }
      // List all app users
      list(params, ...args) {
        const data = params || {};
        const method = this.getMethod();
        return FarolSDK.get(method, args, data, true);
      }

      // Return number of users
      count(params, ...args) {
        const data = params || {};
        const method = this.getMethod('count');
        return FarolSDK.get(method, args, data, true);
      }

      // Get user
      get(entityItemId, ...args) {
        const method = this.getMethod(entityItemId);
        return FarolSDK.get(method, args, null, true);
      }

      // Get user
      getByEmail(email, ...args) {
        const method = this.getMethod('email');
        return FarolSDK.get(method, args, {email: email}, true);
      }

      // Remove specific user
      remove(userId, ...args) {
        const method = this.getMethod(userId);
        return FarolSDK.remove(method, args, null, true);
      }

      // Create new user
      create(user, ...args) {
        const method = this.getMethod();
        return FarolSDK.post(method, args, user, false);
      }

      // Try to log in / authenticate with the user and password
      login(username, password, ...args) {
        const data = {
          username: username,
          password: password,
        };
        const method = FarolSDK.getUrl('auth/user/login');

        return FarolSDK.post(method, args, data, false);
      }

      fields(pushFilter, ...args) {
        const data = {
          pushFilter: pushFilter,
        };
        const method = this.getMethod('field');
        return FarolSDK.post(method, args, data, true);
      }

      validate(token, ...args) {
        const method = this.getMethod(['token_validate', token].join('/'));
        return FarolSDK.post(method, args, null, true);
      }

      resetPassword(email, ...args) {
        const data = {
          email: email,
        };
        const method = this.getMethod('reset_password');
        return FarolSDK.post(method, args, data, false);
      }

      changePassword(token, email, password, ...args) {
        const data = {
          token: token,
          email: email,
          password: password,
        };
        const method = this.getMethod('change_password');
        return FarolSDK.post(method, args, data, false);
      }

      shouldUpdate(userId, ...args) {
        const method = this.getMethod(['should_update', userId].join('/'));
        return FarolSDK.get(method, args, null, true);
      }

      addProject(userId, projectCode, ...args) {
        const method = this.getMethod(
            [userId, 'project', projectCode].join('/')
        );
        return FarolSDK.post(method, args, null, false);
      }

      addProjectRole(userId, projectCode, role, ...args) {
        const method = this.getMethod(
            [userId, 'project', projectCode, 'role', role].join('/')
        );
        return FarolSDK.post(method, args, null, true);
      }

      generateCard(userId, options, ...args) {
        const method = this.getMethod([userId, 'generate_card'].join('/'));
        return FarolSDK.post(method, args, options, true);
      }

      generateBadge(userId, options, ...args) {
        const method = this.getMethod([userId, 'generate_badge'].join('/'));
        return FarolSDK.post(method, args, options, true, 'blob');
      }
    }
  }
}
