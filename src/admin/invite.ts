namespace FarolSDK {
  export namespace Admin {
    export class Invite extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('invite');
      }

      save(shortUrlId, token, email, ...args) {
        const data = {
          token: token,
          shortUrlId: shortUrlId,
          email: email,
        };
        const method = this.getMethod();
        return FarolSDK.post(method, args, data, true);
      }
    }
  }
}
