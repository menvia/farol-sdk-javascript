namespace FarolSDK {
  export namespace Admin {
    export class Entity extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('entity');
      }
      list(entityId, pageSize, pageNumber, ...args) {
        const data = {
          pageSize: pageSize,
          pageNumber: pageNumber,
        };
        const method = this.getMethod(entityId);
        return FarolSDK.get(method, args, data, true);
      }

      // Return number of entity items
      count(entityId, ...args) {
        const method = this.getMethod([entityId, 'count'].join('/'));
        return FarolSDK.get(method, args, null, true);
      }

      // Remove specific entity item
      remove(entityId, entityItemId, ...args) {
        const method = this.getMethod([entityId, entityItemId].join('/'));
        return FarolSDK.remove(method, args, null, true);
      }

      // Insert/update items
      save(entityId, entityItem, ...args) {
        const method = this.getMethod(entityId);
        return FarolSDK.post(method, args, entityItem, true);
      }
    }
  }
}
