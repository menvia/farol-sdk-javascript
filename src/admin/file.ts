namespace FarolSDK {
  export namespace Admin {
    export class File extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('file');
      }

      download(entityId, entityItemId, fileName, params, ...args) {
        const data = params || {};
        const method = this.getMethod(
            [entityId, entityItemId, fileName].join('/')
        );
        return FarolSDK.get(method, args, data, true, 'arraybuffer');
      }

      getUrl(entityId, entityItemId, fileName, format) {
        const data = {
          format: format,
        };
        const method = this.getMethod(
            [entityId, entityItemId, fileName].join('/')
        );
        return FarolSDK.getUrlWithParameters(method, data);
      }

      publicDownload(shortUrlId, fileName, params, ...args) {
        const data = params || {};
        const method = this.getMethod(
            ['public', shortUrlId, fileName].join('/')
        );
        return FarolSDK.get(method, args, data, true, 'arraybuffer');
      }

      publicGetUrl(shortUrlId, fileName, format) {
        const data = {
          format: format,
        };
        const method = this.getMethod(
            ['public', shortUrlId, fileName].join('/')
        );
        return FarolSDK.getUrlWithParameters(method, data);
      }

      // Insert/update items
      upload(
          entityId,
          entityItemId,
          fileName,
          file,
          extension,
          convertToImage,
          publicFile,
          ...args
      ) {
        const data = {
          entityId: entityId,
          entityItemId: entityItemId,
          fileName: fileName,
          file: file,
          convertToImage: convertToImage,
          public_file: publicFile,
        };
        let method = this.getMethod();
        if (extension) {
          method += '?extension=' + extension;
        }
        return FarolSDK.post(method, args, data, true);
      }    }
  }
}
