namespace FarolSDK {
  export namespace Admin {
    export class Device extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('device');
      }
    }
  }
}
