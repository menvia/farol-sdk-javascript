namespace FarolSDK {
  export namespace Admin {
    export class Notification extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('notification');
      }
      // Send push notifications
      send(senderUserId, message, filters, ...args) {
        const data = {
          user_id: senderUserId,
          message: message,
          filters: filters,
        };
        const method = this.getMethod();
        return FarolSDK.post(method, args, data, true);
      }

      list(params, ...args) {
        const data = params || [];
        const method = this.getMethod();
        return FarolSDK.get(method, args, data, true);
      }

      // Return number of notifications
      count(...args) {
        const method = this.getMethod('count');
        return FarolSDK.get(method, args, null, true);
      }
    }
  }
}
