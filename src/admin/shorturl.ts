namespace FarolSDK {
  export namespace Admin {
    export class ShortUrl extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('a');
      }

      getDetails(shortUrlId, ...args) {
        const method = this.getMethod('details/' + shortUrlId);
        return FarolSDK.get(method, args, null, true);
      }
    }
  }
}
