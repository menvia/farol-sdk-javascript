declare let FAROL_ENV: any;
declare let FAROL_API_URL: any;
declare let POSTMAN: any;
declare let pm: any;

namespace FarolSDK {
  export const version = '1.15.10';
  export const authorization = '';
  export const locale = 'en-US';
  export const timezone = 'UTC';
  export const retry = 3;

  export const getUrl = function(baseMethod, method?) {
    let isTestEnvironment = false;
    if (typeof FAROL_ENV !== 'undefined') {
      isTestEnvironment = FAROL_ENV === 'test';
    }

    let apiUrl = 'https://' + (isTestEnvironment ? 'test.' : '') + 'frl.io:443';
    if (typeof FAROL_API_URL !== 'undefined') {
      apiUrl = FAROL_API_URL;
    }

    let url = apiUrl;

    // If there is base method add it
    if (typeof baseMethod !== 'undefined') {
      url += '/' + baseMethod;
    }

    // If there is a method add it
    if (method) {
      url += '/' + method;
    }

    return url;
  };
  export const getUrlWithParameters = function(url, data, verb?) {
    // Check if there are parameters to be sent
    if (
      data !== null &&
      typeof data !== 'undefined' &&
      (verb && verb === 'GET')
    ) {
      url +=
        '?' +
        Object.keys(data)
            .map((k) => {
              if (data[k] !== null && typeof data[k] !== 'undefined') {
                let value = data[k];
                if (Array.isArray(value) && value.length > 0) {
                  value = JSON.stringify(value);
                }
                return encodeURIComponent(k) + '=' + encodeURIComponent(value);
              }
            })
            .join('&');
    }
    return url;
  };
  export const setRetry = function(retry) {
    this.retry = retry;
  };
  export const setAuthorization = function(authorization) {
    this.authorization = authorization;
  };
  export const setLocale = function(locale) {
    this.locale = locale;
  };
  export const setTimezone = function(timezone) {
    this.timezone = timezone;
  };
  export const getHeader = (key, value) => {
    return {
      key: key,
      value: value,
    };
  };
  export const getLocaleHeader = function() {
    return this.getHeader('Accept-Language', this.locale);
  };
  export const getTimezoneHeader = function() {
    return this.getHeader('Time-Zone', this.timezone);
  };
  export const getAuthorizationHeader = function() {
    return this.getHeader('Authorization', this.authorization);
  };
  export const getClientVersionHeader = function() {
    const clientVersion = 'Farol/' + this.version + '/javascript';
    return this.getHeader('X-Client-Version', clientVersion);
  };
  // Look for the callback function in arguments
  export const getCallback = (originArguments) => {
    // loop all the arguments
    for (const index in originArguments) {
      // The first one to be a function is considered the callback
      if (typeof originArguments[index] === 'function') {
        return originArguments[index];
      }
    }
  };
  export const request = function(
      verb,
      url,
      originArguments,
      data,
      needsAuthorizationHeader,
      responseType
  ) {
    data = data || {};
    url = this.getUrlWithParameters(url, data, verb);

    // Get callback function
    const callback = this.getCallback(originArguments);

    // If there is no callback return a promise
    if (callback === undefined) {
      return new Promise((resolve, reject) => {
        FarolSDK.sendRequest(
            verb,
            url,
            originArguments,
            data,
            needsAuthorizationHeader,
            responseType,
            (status, response) => resolve({status: status, response: response})
        );
      });
    } else {
      return this.sendRequest(
          verb,
          url,
          originArguments,
          data,
          needsAuthorizationHeader,
          responseType,
          callback
      );
    }
  };
  export const sendRequest = function(
      verb,
      url,
      originArguments,
      data,
      needsAuthorizationHeader,
      responseType,
      callback,
      currentRetry = 0
  ) {
    // use it in postman
    if (typeof POSTMAN !== 'undefined' && POSTMAN) {
      const postRequest = {
        url: url,
        method: verb,
        body: {
          mode: 'raw',
          raw: JSON.stringify(data),
        },
        header: {
          'Authorization': this.authorization,
          'Content-Type': 'application/json',
          'X-Client-Version': 'Farol/' + this.version + '/javascript/postman',
        },
      };
      pm.sendRequest(postRequest, callback);
    } else {
      let request = undefined;

      // IF CommonJS require xmlhttprequest module
      if (typeof module !== 'undefined' && module.exports) {
        const XMLHttpRequester = require('xmlhttprequest').XMLHttpRequest;
        request = new XMLHttpRequester();
      } else {
        request = new XMLHttpRequest();
      }

      // If data.export_to file will be returned a response array buffer shall
      // be returned
      if (data && data.export_to) {
        request.responseType = 'blob';
      } else if (responseType) {
        request.responseType = responseType;
      }

      request.open(verb, url, true);
      const headers = [
        this.getLocaleHeader(),
        this.getTimezoneHeader(),
        this.getClientVersionHeader(),
      ];
      if (needsAuthorizationHeader) {
        headers.push(this.getAuthorizationHeader());
      }
      for (let i = 0; i < headers.length; i++) {
        const header = headers[i];
        request.setRequestHeader(header.key, header.value);
      }
      if (data && data.export_to) {
        request.onload = function(e) {
          if (this.status == 200) {
            const exportTo = data.export_to;
            const mimeType =
              'application/' + (data.export_to === 'xlsx')
                ? 'vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                : 'pdf';
            // Create a new Blob object using the response data of the
            // onload object
            const blob = new Blob([this.response], {
              type: mimeType,
            });

            // Create a DOMString representing the blob
            const url = window.URL.createObjectURL(blob);

            // Create a link element, hide it, direct it towards the blob, and
            // then 'click' it programatically
            const a = document.createElement('a');
            a.style.display = 'none';
            document.body.appendChild(a);

            // Point the link to the blob file
            a.href = url;

            // Define filename
            let filename = 'download.' + exportTo;
            const disposition = this.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
              const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
              const matches = filenameRegex.exec(disposition);
              if (matches != null && matches[1]) {
                filename = matches[1].replace(/['"]/g, '');
              }
            }
            a.download = filename;

            // programatically click the link to trigger the download
            a.click();
            // release the reference to the file by revoking the Object URL
            window.URL.revokeObjectURL(url);
          }
          callback(request.status, 'blob');
        };
      } else {
        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            if (request.status >= 500 && currentRetry < retry) {
              sendRequest(
                  verb,
                  url,
                  originArguments,
                  data,
                  needsAuthorizationHeader,
                  responseType,
                  callback,
                  ++currentRetry
              );
            } else {
              let response = request.response;
              // NODEJS lib xmlhttprequest uses request.responseText instead
              // of request.response
              if (request.response === undefined) {
                response = request.responseText;
              }
              if (!data || (data && !data.export_to && !responseType)) {
                response = FarolSDK.parseResult(response);
              }
              callback(request.status, response);
            }
          }
        };
      }

      // Trigger the request (if not GET and has data, it will send data on
      // the body as JSON)
      if (verb !== 'GET' && data) {
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(data));
      } else {
        request.send();
      }
    }
  };

  /**
   * Get parameter
   * @param  {[type]} result the received result
   * @return {[type]}        return it parsed
   */
  export const parseResult = function(result) {
    if (result) {
      try {
        result = JSON.parse(result);
      } catch (e) {
        // That is not a JSON
      }
    }
    return result;
  };

  export const get = function(
      url,
      originArguments,
      data,
      needsAuthorizationHeader,
      responseType?
  ) {
    return this.request(
        'GET',
        url,
        originArguments,
        data,
        needsAuthorizationHeader,
        responseType
    );
  };
  export const post = function(
      url,
      originArguments,
      data,
      needsAuthorizationHeader,
      responseType?
  ) {
    return this.request(
        'POST',
        url,
        originArguments,
        data,
        needsAuthorizationHeader,
        responseType
    );
  };
  export const put = function(
      url,
      originArguments,
      data,
      needsAuthorizationHeader,
      responseType?
  ) {
    return this.request(
        'PUT',
        url,
        originArguments,
        data,
        needsAuthorizationHeader,
        responseType
    );
  };
  export const remove = function(
      url,
      originArguments,
      data,
      needsAuthorizationHeader,
      responseType?
  ) {
    return this.request(
        'DELETE',
        url,
        originArguments,
        data,
        needsAuthorizationHeader,
        responseType
    );
  };
}
