namespace FarolSDK {
  export namespace Product {
    export class Product extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('event/event');
      }

      /**
       * List items
       * @param {String} userId User ID
       * @param {Object} params Parameters
       * @return {Object}       Promise
       */
      list(userId, params, ...args) {
        const data = params || {};
        data.owner = userId;
        const method = this.getMethod('dash');
        return FarolSDK.get(method, args, data, true);
      }

      publicList(product, ...args) {
        const data = {
          product: product,
        };
        const method = this.getMethod('public');
        return FarolSDK.get(method, args, data, true);
      }

      // Return number of entity items
      count(userId, eventType, ...args) {
        const data = {
          userId: userId,
          event_type: eventType,
        };
        const method = this.getMethod(['dash', 'count'].join('/'));
        return FarolSDK.get(method, args, data, true);
      }

      /**
       * Get item
       * @param {String} itemId Item ID
       * @param {String} userId User ID
       * @param {Object} params Parameters
       * @return {Promise}
       */
      get(itemId, userId, params, ...args) {
        const data = params || {};
        data.user_id = userId;
        const method = this.getMethod(itemId);
        return FarolSDK.get(method, args, data, true);
      }

      // Insert/update items
      save(eventItem, ...args) {
        const method = this.getMethod();
        return FarolSDK.post(method, args, eventItem, true);
      }

      // update existing item
      update(itemId, item, ...args) {
        const data = item;
        const method = this.getMethod(itemId);
        return FarolSDK.put(method, args, data, true);
      }

      getDetails(project, ...args) {
        const method = this.getMethod(['details', project].join('/'));
        return FarolSDK.get(method, args, {}, true);
      }
    };
  }
}
