namespace FarolSDK {
  export namespace Product {
    export class MatchmakingSpot extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('matchmaking_spot');
      }
    }
  }
}
