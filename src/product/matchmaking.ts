namespace FarolSDK {
  export namespace Product {
    export class Matchmaking extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('matchmaking');
      }

      /**
       * Match
       * @param  {...any} args
       * @return {Promise}
       */
      match(...args) {
        const method = this.getMethod('match');
        return FarolSDK.get(method, args, null, false);
      }
    }
  }
}
