namespace FarolSDK {
  export namespace Product {
    export class MatchmakingBuyer extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('matchmaking_buyer');
      }
    }
  }
}
