namespace FarolSDK {
  export namespace Product {
    export class Category extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('event/category');
      }
    }
  }
}
