namespace FarolSDK {
  export namespace Product {
    export class Exhibitor extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('event/exhibitor');
      }
    }
  }
}
