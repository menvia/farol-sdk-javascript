namespace FarolSDK {
  export namespace Product {
    export class Registration extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('event/registration');
      }

      /**
       * Update item
       * @param {Object} registrationId Registration ID
       * @return {Promise}
       */
      checkin(registrationId, ...args) {
        const data = {
          registration_id: registrationId
        };
        const method = this.getMethod('checkin');
        return FarolSDK.post(method, args, data, true);
      }

      listLeadCapture(...args) {
        const method = this.getMethod('listLeadCapture');
        return FarolSDK.get(method, args, null, true);
      }
    }
  }
}
