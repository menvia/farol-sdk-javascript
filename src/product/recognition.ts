namespace FarolSDK {
  export namespace Product {
    export class Recognition extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('event/recognition');
      }
    }
  }
}
