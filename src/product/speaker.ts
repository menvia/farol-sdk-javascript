namespace FarolSDK {
  export namespace Product {
    export class Speaker extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('event/speaker');
      }

      /**
       * List public items
       * @param {String}   token Event token
       * @return {Promise}       Promise
       */
      publicList(token, ...args) {
        const method = this.getMethod(['public', token].join('/'));
        return FarolSDK.get(method, args, null, false);
      }
    }
  }
}
