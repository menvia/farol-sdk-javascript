namespace FarolSDK {
  export namespace Survey {
    export class Answers extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('survey/answer');
      }
      answered(userId, ...args) {
        const method = this.getMethod(['answered', userId].join('/'));
        return FarolSDK.get(method, args, null, true);
      }
    }
  }
}
