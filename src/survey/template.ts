namespace FarolSDK {
  export namespace Survey {
    export class Template extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('survey/template');
      }
    }
  }
}
