class AbstractClass {
  baseMethod = '';
  /**
   * Abstract class constructor
   * @param {String} baseMethod Base method string
   */
  constructor(baseMethod) {
    this.baseMethod = baseMethod;
  }

  /**
   * Get method URL based on the base method
   * @param  {String} method method string
   * @return {String}        API URL
   */
  getMethod(method?) {
    return FarolSDK.getUrl(this.baseMethod, method);
  }

  /**
   * List items
   * @param {String} userId User ID
   * @param {Object} params Parameters
   * @return {Object}       Promise
   */
  list(userId, params, ...args) {
    const data = params || {};
    data.user_id = userId;
    const method = this.getMethod();
    return FarolSDK.get(method, args, data, true);
  }

  /**
   * Count items
   * @param {String} userId User ID
   * @param {Object} params Parameters
   * @return {Promise}
   */
  count(userId, params, ...args) {
    const data = params || {};
    data.user_id = userId;
    const method = this.getMethod('count');
    return FarolSDK.get(method, args, data, true);
  }

  /**
   * Get item
   * @param {String} itemId Item ID
   * @param {String} userId User ID
   * @param {Object} params Parameters
   * @return {Promise}
   */
  get(itemId, userId, params, ...args) {
    const data = params || {};
    data.user_id = userId;
    const method = this.getMethod(itemId);
    return FarolSDK.get(method, args, data, true);
  }

  /**
   * Get item
   * @param {String} userId Item ID
   * @param {Object} params Parameters
   * @return {Promise}
   */
  listMine(userId, params, ...args) {
    const data = params || {};
    const method = this.getMethod(['mine', userId].join('/'));
    return FarolSDK.get(method, args, data, true);
  }

  /**
   * Create item
   * @param {Object} item Item
   * @return {Promise}
   */
  create(item, ...args) {
    const method = this.getMethod();
    return FarolSDK.post(method, args, item, true);
  }

  /**
   * Update item
   * @param {Object} item Item
   * @return {Promise}
   */
  update(item, ...args) {
    const method = this.getMethod(item._id);
    return FarolSDK.put(method, args, item, true);
  }

  /**
   * Remove Item
   * @param {String} itemId Item ID
   * @return {Promise}
   */
  remove(itemId, ...args) {
    const method = this.getMethod(itemId);
    return FarolSDK.remove(method, args, {}, true);
  }

  /**
   * Get item
   * @param {String} itemExtId Item ID
   * @return {Promise}
   */
  getByExtId(itemExtId, ...args) {
    const method = this.getMethod(['extid', itemExtId].join('/'));
    return FarolSDK.get(method, args, null, true);
  }

  /**
   * Update item
   * @param {Object} item Item
   * @return {Promise}
   */
  saveByExtId(item, ...args) {
    const method = this.getMethod('extid');
    return FarolSDK.post(method, args, item, true);
  }
}
