namespace FarolSDK {
  export namespace Finance {
    export class Coupon extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('finance/coupon');
      }
    }
  }
}
