namespace FarolSDK {
  export namespace Finance {
    export class Modifier extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('finance/modifier');
      }

      list(params, ...args) {
        const data = params || {};

        const method = this.getMethod();
        return FarolSDK.get(method, args, data, true);
      }

      // Return number of users
      validate(code, items, ...args) {
        const params = {
          code: code,
          items: items,
        };
        const method = this.getMethod('validate');
        return FarolSDK.post(method, args, params, true);
      }

      // Use a specific modifier type 6
      use(userId, code, ...args) {
        const params = {
          user_id: userId,
          code: code,
        };
        const method = this.getMethod('use');
        return FarolSDK.post(method, args, params, true);
      }
    }
  }
}
