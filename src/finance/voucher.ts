namespace FarolSDK {
  export namespace Finance {
    export class Voucher extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('finance/voucher');
      }

      /**
       * Notify owner
       * @param {String}   itemId
       * @return {Promise}
       */
      notifyOwner(itemId) {
        const method = this.getMethod([itemId, 'notify_owner'].join('/'));
        return FarolSDK.get(method, null, null, true);
      }

      /**
       * Notify recipient
       * @param {String}   itemId
       * @return {Promise}
       */
      notifyRecipient(itemId) {
        const method = this.getMethod([itemId, 'notify_recipient'].join('/'));
        return FarolSDK.get(method, null, null, true);
      }
    }
  }
}
