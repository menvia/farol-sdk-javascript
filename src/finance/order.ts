namespace FarolSDK {
  export namespace Finance {
    export class Order extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('finance/order');
      }

      listByUser(userId, entityId, ...args) {
        const data = {
          entityId: entityId,
        };
        const method = this.getMethod(['user', userId].join('/'));
        return FarolSDK.get(method, args, data, true);
      }

      changeStatus(params, ...args) {
        const data = params || {};
        const method = this.getMethod('status');
        return FarolSDK.post(method, args, data, true);
      }
    }
  }
}
