namespace FarolSDK {
  export namespace Finance {
    export class Transaction extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('finance/transaction');
      }

      /**
       * Execute the paypal payment
       * @param {ObjectId} paymentId
       * @param {ObjectId} payerId
       * @param {array} args
       * @return {Promise}
       */
      executePaypal(paymentId, payerId, ...args) {
        const params = {
          payment_id: paymentId,
          payer_id: payerId,
        };
        const method = this.getMethod('paypal_execute');
        return FarolSDK.post(method, args, params, true);
      }
    }
  }
}
