namespace FarolSDK {
  export namespace Finance {
    export class ConsumerProfile extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('finance/consumer_profile');
      }
    }
  }
}
