namespace FarolSDK {
  export namespace Certification {
    export class Certificate extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('certification/certificate');
      }
    }
  }
}
