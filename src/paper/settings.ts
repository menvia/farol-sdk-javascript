namespace FarolSDK {
  export namespace Paper {
    export class Settings extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('paper/settings');
      }
    }
  }
}
