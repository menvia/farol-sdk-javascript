namespace FarolSDK {
  export namespace Paper {
    export class Category extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('paper/category');
      }
    }
  }
}
