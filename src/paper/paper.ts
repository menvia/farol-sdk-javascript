namespace FarolSDK {
  export namespace Paper {
    export class Paper extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('paper/paper');
      }

      /**
       * List public submission by code
       * @param {String}   productCode Product Code
       * @return {Promise}             Promise
       */
      publicList(productCode, ...args) {
        const data = {
          product_code: productCode,
        }
        const method = this.getMethod('public');
        return FarolSDK.get(method, args, data, false);
      }

      /**
       * Get public submission by id
       * @param {String}   itemId Item id
       * @return {Promise}        Promise
       */
      publicGet(itemId, ...args) {
        const method = this.getMethod(['public', itemId].join('/'));
        return FarolSDK.get(method, args, null, false);
      }

      /**
       * List public items
       * @param {String}   itemId Item id
       * @return {Promise}       Promise
       */
      listReviewers(itemId, ...args) {
        const method = this.getMethod([itemId, 'reviewers'].join('/'));
        return FarolSDK.get(method, args, null, true);
      }
    }
  }
}
