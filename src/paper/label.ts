namespace FarolSDK {
  export namespace Paper {
    export class Label extends AbstractClass {
      /**
       * Class constructor
       */
      constructor() {
        super('paper/label');
      }
    }
  }
}
