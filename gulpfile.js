const gulp = require('gulp');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const umd = require('gulp-umd');
const pump = require('pump');
const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');

const job = function(cb, debug) {
  const pumpSteps = [
    tsProject.src(),
    tsProject(),
    babel({
      presets: ['@babel/preset-env'],
      plugins: [
        '@babel/plugin-transform-strict-mode',
        '@babel/plugin-transform-async-to-generator',
      ],
      ignore: ['dist/', 'node_modules'],
    }),
    concat('farol-sdk.js'),
    umd({
      exports: function(file) {
        return 'FarolSDK';
      },
      namespace: function(file) {
        return 'FarolSDK';
      },
    }),
  ];
  // If it is not in debug it will also uglify
  if (!debug) {
    pumpSteps.push(uglify());
  }
  pumpSteps.push(gulp.dest('dist'));
  pump(pumpSteps, cb);
};

const debug = function(cb) {
  job(cb, true);
};

const production = function(cb) {
  job(cb, false);
};

gulp.task('default', production);
gulp.task('debug', debug);
